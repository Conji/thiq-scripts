# Jobs
This directory is solely dedicated to the jobs section of the server. The jobs plugin's goal is to completely revamp gameplay within the server. While all crafting materials remain the same, some materials have been added to facilitate the jobs.

### The Basics  
When you first start on a server, the first thing you do is typically go punch a couple trees and begin building your base. That's not the case with Jobs. While it's completely possible to do it on your own, it's not recommended because you won't get very far for long. Before too long (as soon as possible, recommendedly), you'll need to decide on a job and then apply to a company. If you don't see one you like, you can make your own. Know it'll be hard to get one running though. 

## Game Changes
While jobs give a simple change to the server, in order for a job to actually seem useful, some core mechanics have to be adjusted. This includes how players interact with the world and how the world interacts with it. 

*Gathering wood*: this core mechanic has been finely tuned to bring more meaning to the lumberjack job. Punching a tree with your bare hands no longer yields a wooden log, but rather a chance of a plank of that tree type. This means that breaking a log may not actually drop a plank. 

*Mining*: as with breaking logs, the mining system also varies on the miner job. Without it, blocks mined may not yield any drops. The chances decrease as the block value increases. So while a 1 in 5 mines of a stone block will drop cobblestone, around 1 in 10 coal blocks will yield coal, and so on.

*Tool crafting*: all tools made will be less than perfect if you are not a smith. The remaining durability of all tools, armor, and weapons, will be 25%. If you are a smither, then the resulting durability increases as your level increases. 

*Baking*: all recipes have a chance of burning, resulting in a pile of ash (specifically gunpowder).

*Potions*: all potions have a chance of fluking, or resulting in a bottle of water.

*Farming*: if you aren't a farmer, you won't be able to till land for planting crops.

*Player reach*: one of the biggest changes, players can no longer place or destroy blocks more than 3 blocks away.

## Job Types
### Miner
Made in mind for the typical miner, the miner job allows for more predictable drops and farther reach while mining. This means less chance of a block not yielding any drops. As you get higher in the levels, you begin unlocking more tools that allow for faster and more efficient mining such as drills, mining charges, ore sensors, quarries, and more.

### Builder
Built around the builder, this job lets you create at a faster rate than typical gameplay. As you level up, your reach is extended and your block efficiency is greater. This means there's a chance you can place a block and it not use a block in your inventory. With further progression, you can gain access to blueprint tables, automated builders, larger chests, and more.

### Farmer
With the core change of not being able to till dirt, the farmer shines the most when it comes to group stability. They'll be able to till land, plant crops, and harvest them. As they level up, crops will yield more results and you'll be able to plant new crops. Higher in the tiers grants access to automated farming machines, animals following you willingly, farm assisting tools such as sun blocks, and more.

### Lumberjack
The lumberjack is straight forward. Their job is the gather wood from trees for everyone else to use. While it seems simple, this job is key to any company being successful. As you advance in levels, you'll have farther reach, yield more drops, timber physics (trees fall automatically at the point of break), automated tree farms, and more.

### Baker
An expert at all things food, your main job will be to ensure that the company is eating the good old food your mother used to make. You'll burn recipes less often and eventually, never. You'll gain access to new recipes as well that allow for more health, more variety, and a little piece of home. As you gain more experience, your recipes will heal and saturate more.

### Hunter  

### Fisherman  

### Salesman  

### Alchemist  

### Smith  

### Explorer  

## Companies  
Jobs has what's called 'companies'. Similar to regular companies, there is a CEO - the person that started the company. Each worker is paid off of commission though, so what you earn for doing your job, you get to keep. The only thing keeping the company alive is your willingness to work and how well built your company is. 
