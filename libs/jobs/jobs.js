/*
A plugin for jobs that interacts with the economy plugin. Jobs are divided into several categories:
- miner (money gained from mining stone and ores)
- lumberjack (money gained from mining wood based items)
- farmer (money gained from growing successful crops)
- baker (money gained from baking and cooking food)
- hunter (money gained from killing mobs and animals)
- salesman (money gained from recieving money and trading with villagers)
- fisherman (money gained from fishing)
- builder (money gained from building)
- alchemist (money gained from brewing potions)
- smith (money gained from crafting tools, weapons, and armor)
- explorer (money gained from walking, swimming, and boat riding)
 */
(function() {

    var Gui = require('gui');

    var jobs = [];
    // load all job types
    loadLibraryScript('jobs/types/miner.job');
    loadLibraryScript('jobs/types/builder.job');
    loadLibraryScript('jobs/types/lumberjack.job');
    loadLibraryScript('jobs/types/farmer.job');

    loadLibraryScript('jobs/loader');
    var jobsLoader = new JobsLoader();

    var onlinePlayers = Bukkit.getOnlinePlayers();
    for (var i = 0; i < onlinePlayers.length; i++) {
        jobsLoader.loadProfile(onlinePlayers[i].getUniqueId());
    }

    function getJobPosting(job) {
        var posting = itemStack(job.jobPostingItem, 1, 0);
        var postingMeta = posting.itemMeta;
        postingMeta.displayName = job.name;
        var lore = job.jobPostingInfo;
        lore.push('');
        lore.push('\xA7nJob Tree');
        for (var i = 0; i < job.jobTree.length; i++) {
            lore.push('\xA79- ' + job.jobTree[i]);
        }
        postingMeta.setLore(lore);
        posting.itemMeta = postingMeta;
        return posting;
    }

    function assignJob(player, job) {
        var profile = JobsLoader.getProfile(player.getUniqueId());
        profile._job = job.id;
        profile._lvl = 1;
        JobsLoader.saveProfile(player.getUniqueId());
    }

    var jobsGui = new Gui();
    jobsGui.setTitle('Jobs Listing');

    function handleJobSelection(player, job) {
        player.sendMessage("You've taken a job as a " + job.name + "!");
        assignJob(player, job);
        jobsGui.close(player);
    }

    for (var i = 0; i < jobs.length; i++) {
        var job = jobs[i];
        jobsGui.add(getJobPosting(job), null, function(player) { handleJobSelection(player, job); });
    }

    registerCommand({
        name: 'jobs',
        description: 'Does jobs stuff',
        usage: "\xA7eUsage: /<command>",
        permission: registerPermission('jobs.use', true),
        permissionMessage: "\xA7cYou do not have sufficient permissions to use that."
    }, function(sender, label, args) {
        var profile = JobsLoader.getProfile(sender.getUniqueId());
        if (args.length == 0) {
            jobsGui.open(sender);
            return;
        }
        if (profile._job == -1) {
            sender.sendMessage('You currently do not have a job! You can choose one by typing /jobs');
            return;
        }
        var job = jobs[profile._job];
        var factory = factories[profile._job];
        switch (args[0]) {
            case 'info':
                sender.sendMessage('Level ' + profile._lvl + ' ' + job.name);
                sender.sendMessage('Current XP: ' + profile._jobXp);
                return;
            case 'xp':
                return;
            default:
                return;
        }
    });
})();