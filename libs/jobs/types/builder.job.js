/*
BUILDER:

SKILL TREE:
Levels 1-5: when building, you are able to place blocks further away.
Levels 6-15: increase in pay as you build
Levels 16+: special perks like material efficiency, cheaper resources, and access to the blueprint table.
*/

function BuilderJob() {
    jobs[1] = this;
    this.name = '\xA73Builder';
    this.id = 1;
    this.jobPostingItem = 45;
    this.jobPostingInfo = [
        "Have an eye for design?",
        "Come build with us!",
        "\xA72- Greater reach when building",
        "\xA72- Less resource usage when crafting materials"
    ];
    this.jobTree = [
        'Carpenter', 'Designer', 'Builder', 'Architect', 'Wright'
    ]
    return this;
}