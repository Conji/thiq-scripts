/*
FARMER:

SKILL TREE:
Levels 1-5: when collecting crops, you have a higher chance at drops
Levels 6-15: collect more money for farming
Levels 16+: special perks like automated farms and higher drops
*/

function FarmerJob() {
    jobs[3] = this;
    this.name = '\xA7aFarmer';
    this.id = 3;
    this.jobPostingItem = 3;
    this.jobPostingInfo = [
        "Have a green thumb?",
        "Show them the world needs farmers!",
        "\xA72- Grow crops at faster rates",
        "\xA72- More drops when farming"
    ];
    this.jobTree = [
        'Hobbyist', 'Garden Maid', 'Farmer', 'Nurterer', 'Green Giant'
    ];
    return this;
}