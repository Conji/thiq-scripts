/*
LUMBERJACK:

SKILL TREE:
Levels 1-5: when breaking wood based blocks, more predictability on the drop
Levels 6-15: increase in pay as you cut wood
Levels 16+: special perks like cutting down full trees,
*/

function LumberJack() {
    jobs[2] = this;
    this.name = '\xA7dLumberjack';
    this.id = 2;
    this.jobPostingItem = 5;
    this.jobPostingInfo = [
        "Can you wield an axe?",
        "Lets put those burly shoulders to use!",
        "\xA72- Cut wood more efficiently",
        "\xA72- Better proficiency with the axe"
    ];
    this.jobTree = [
        'Small Fry', 'Chopper', 'Lumberjack', 'Tree Master', 'Deforester'
    ];
    return this;
}