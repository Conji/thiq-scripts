/*
MINER:

SKILL TREE:
Levels 1-5: when mining, there is more consistency on what is mined.
Levels 6-15: increase in pay while mining
Levels 16+: greater tool efficiency, mining speed, and greater chance at increased loot drops.
*/

function MinerJob() {
    jobs[0] = this;
    this.name = '\xA7cMiner';
    this.id = 0;
    this.jobPostingItem = 1;
    this.jobPostingInfo = [
        "Want to get paid for mining?",
        "Then we have the job for you!",
        "\xA72- Mine faster and get more rewards",
        "\xA72- More predictable drops"
    ]
    this.jobTree = [
        'Apprentice', 'Digger', 'Miner', 'Drill', 'Miner Miner 49er'
    ]
    return this;
}