/* In charge of loading the stored data and such for jobs. */

var fs = require('fs');

var dataLocation = './plugins/Thiq/libs/jobs/data/';
fs.mkdirs(dataLocation); // initialize to ensure it exists

/**
 * @typedef {Object} JobsProfile A Jobs profile of the player
 * @param {string} uuid 
 * @param {number} job 
 * @param {number} lvl 
 * @param {number} jobXp
 * @returns {JobsProfile}
 */
function JobsProfile(uuid, job, lvl, jobXp) {
    this._player = uuid;
    this._job = job || -1;
    this._lvl = lvl || 0;
    this._jobXp = jobXp || 0;

    this.save = function() {
        JobsLoader.saveProfile(this);
    }

    return this;
}

function JobsLoader() {
    var _loadedProfiles = {};

    this.loadProfile = function(uuid) {
        var profile = null;
        if (fs.exists(dataLocation + uuid + '.json')) {
            try {
                profile = JSON.parse(fs.read(dataLocation + uuid + '.json'));
            } catch (exception) {
                log('Failed to load profile for ' + uuid, 'c');
                profile = new JobsProfile(uuid);
            }
        } else {
            profile = new JobsProfile(uuid);
            fs.write(dataLocation + uuid + '.json', JSON.stringify(profile));
        }

        _loadedProfiles[uuid] = profile;
        return profile;
    }

    this.saveProfile = function(uuid) {
        var profile = JobsLoader.getProfile(uuid);
        fs.write(dataLocation + uuid + '.json', JSON.stringify(profile));
        return profile;
    }

    this.getProfile = function(uuid) {
        var profile = null;
        if (_loadedProfiles[uuid] == undefined) {
            profile = JobsLoader.loadProfile(uuid);
        } else {
            profile = _loadedProfiles[uuid];
        }
        return profile;
    }

    return this;
}

registerEvent(player, 'join', function(event) {
    JobsLoader.loadProfile(event.getPlayer().getUniqueId());

});

registerEvent(player, 'quit', function(event) {
    JobsLoader.saveProfile(event.getPlayer().getUniqueId());
});