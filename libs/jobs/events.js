/* All events that Jobs fires so that other scripts can hook into it. */

// PLAYER EVENTS
//# jobAssignedEvent
//# event: { getPlayer(), getProfile() }
registerEvent(player, 'jobAssigned', function (event) {
});

//# jobLevelIncreasedEvent
//# event: { getPlayer(), getProfile() }
registerEvent(player, 'jobLevelIncreased', function (event) {
});
