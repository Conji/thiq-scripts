/**
 * @typedef {MinerFactory}
 * @param {Factory} jobsFactoryInstance
 */
var MinerFactory = function(jobsFactoryInstance) {
    jobsFactoryInstance.createHandle('blockBreak', function(player, profile) {
        jobsFactoryInstance.incrementLevelFor(player, profile, 1,
            function(level) { return level * 10; },
            function(player) {
                player.playEffect(org.bukkit.Effect.CHORUS_FLOWER_GROW);
            });
    });

    return this;
}