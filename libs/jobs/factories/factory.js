function Factory() {
    var _handledEvents = {};

    this.incrementLevelFor = function(player, profile, incrementBy, xpReqFn, lvlUpFn) {
        profile._jobXp += incrementBy;
        var requiredXpForLevelUp = xpReqFn(profile._lvl);
        if (requiredXpForLevelUp <= profile._jobXp) {
            var newXp = profile._jobXp % requiredXpForLevelUp;
            var newLevel = profile._jobXp / requiredXpForLevelUp;
            profile._jobXp = newXp;
            profile._lvl += newLevel;
            player.sendMessage('\xA72You are now a level \xA7e' + profile._lvl + ' ' + jobs[profile._job].name + '!');
            if (lvlUpFn) {
                lvlUpFn(player);
            }
            profile.save();
        }
        return this;
    }

    this.createHandle = function(eventName, callback) {
        if (_handledEvents[eventName] === undefined) {
            _handledEvents[eventName] = [];
        }
        _handledEvents[eventName].push(callback);
        return this;
    }

    this.handle = function(eventName, player, profile) {
        if (_handledEvents[eventName] === undefined) return;
        for (var i = 0; i < _handledEvents[eventName].length; i++) {
            _handledEvents[eventName][i](player, profile);
        }
        return this;
    }

    return this;
}