var registeredChairs = [];

registerEvent(player, 'interact', function(event) {
    if (event.action != importClass('org.bukkit.event.block.Action').RIGHT_CLICK_BLOCK ||
        event.getClickedBlock().type != org.bukkit.Material.WOOD_STAIRS || event.player.isSneaking()) {
        return;
    }
    event.cancelled = true;
    var target = event.player.getTargetBlock(null, 10);
    var oldLocation = event.player.getLocation();
    location = new org.bukkit.Location(event.player.world, target.x + 0.5, target.y - 1, target.z + 0.5);
    en = event.player.world.spawn(location, org.bukkit.entity.ArmorStand.class, function(e) {
        e.setSilent(true);
        e.setVisible(false);
    });
    en.addPassenger(event.player);
    setInterval(function(cancel) {
        if (en.getPassengers().length == 0) {
            event.player.teleport(oldLocation);
            en.remove();
            cancel();
        }
    }, 100);
});