minecraft_item_regexes = [
  [/^air$/i, 0]
  [/^stone$/i, 1]
  [/^grass$/i, 2]
  [/^dirt$/i, 3]
  [/^cobble(stone)?$/i, 4]
  [/plank(s)?$/i, 5]
  [/^sap/i, 6]
  [/^bedrock$/i, 7]
  [/^(still)?(.*)water$/i, 8]
  [/^(sta)?(.*)water$/i, 9]
  [/^lava$/i, 10]
  [/^(sta)?(.*)lava$/i, 11]
  [/^sand$/i, 12]
  [/^gravel$/i, 13]
  [/^gold(_)?ore$/i, 14]
  [/^iron(_)?ore$/i, 15]
  [/^coal(_)?ore$/i, 16]
  [/^(wood|log)$/i, 17]
  [/^lea(f|ve)(s)?$/i, 18]
  [/^sponge$/i, 19]
  [/^glass$/i, 20]
  [/^lapis(.*)ore$/i, 21]
  [/^lapis/i, 22]
  [/^dispenser$/i, 23]
  [/^sandstone$/i, 24]
  [/^note/i, 25]
# [/^bed(.*)block$/i, 26]
  [/^power(.*)(rail|track)$/i, 27]
  [/^detect(.*)(rail|track)$/i, 28]
  [/^stick(.*)piston$/i, 29]
  [/web$/i, 30]
  [/^(tallgrass|shrub|fern)$/i, 31]
  [/^deadshrub$/i, 32]
  [/^piston$/i, 33]
# [/^piston(.*)head$/i, 34]
  [/^wool$/i, 34]

  [/^redstone(_)?lamp$/i, 123]

  [/^ink/i, 351]
]

cloneLocation = (location, args) ->
  loc = location.clone()
  for k,v of args
    loc[k] = v if v?
  return loc

safeTeleport = (entity, location) ->
  checkTeleport entity
  location = GroundFinder::suitableGround location
  throw "No safe location" if not location
  entity.teleport location

fixItemMetaColors = (dataItem) ->
  if dataItem instanceof org.bukkit.entity.Player
    player = dataItem
    return fixItemMetaColors player.inventory
  else if dataItem instanceof org.bukkit.inventory.Inventory
    inventory = dataItem
    for item in _a inventory
      fixItemMetaColors item
    return
  else if dataItem instanceof org.bukkit.inventory.ItemStack
    item = dataItem
    meta = item.itemMeta

    return unless meta?

    badCharPattern = /[\u00C2\u00C3\u00C6\u0192\u2019\u201A]/g

    if meta.displayName?
      meta.displayName = _s(meta.displayName).replace badCharPattern, ''

      if meta.displayName.equals 'null'
        meta.displayName = null

    if meta.lore?
      meta.lore = for l in _a meta.lore
        _s(l).replace badCharPattern, ''

    item.itemMeta = meta
    return

registerCommand
  name: "fixitemcolors",
  description: "Fixes dem item colors that contain a \xC2",
  usage: "/<command>",
  (sender, label, args) ->
    fixItemMetaColors sender
    sender.sendMessage "Hopefully it's fixed now :)"

registerEvent player, 'join', (event) ->
  bukkit_sync ->
    fixItemMetaColors event.player

registerEvent player, 'teleport', (event) ->
  fixItemMetaColors event.player

enumFind = (_enum, value) ->
  value = _s(value).toUpperCase().replace /[_]/i, ''
  for k,v of _enum
    name = _s k
    name = name.toUpperCase().replace /[_]/i, ''
    if name == value
      return v
  undefined

getItemId = (value) ->
  return value.id if value instanceof org.bukkit.Material
  return new Number(value) unless isNaN value
  envalue = enumFind org.bukkit.Material, value
  return envalue.id if envalue
  for item in minecraft_item_regexes
    return item[1] if item[0].test(value)
  return -1

itemStack = (id, amount, data, meta) ->
  id = getItemId(id)
  item = new org.bukkit.inventory.ItemStack(id)
  item.amount = amount  if amount
  item.durability = data  if data
  item.itemMeta = meta  if meta
  return item
class BlockInfo
  dangerousItems = [8, 9, 10, 11, 30, 81]
  constructor: (block) ->
    block = block.block if block instanceof org.bukkit.Location
    @id = new Number(block.typeId)
    @data = new Number(block.data)
    @solid = block.type.solid
    @dangerous = arrayContains dangerousItems, @id
    @getBlock = () -> block
nearestEntity = (searchEntity, type) ->
  searchL = if searchEntity instanceof org.bukkit.Location
    searchEntity
  else searchEntity.location
  target = null

  entities = _a searchEntity.world.entities

  entities.splice(entities.indexOf(searchEntity), 1)

  if type
    type = org.bukkit.entity[type] if typeof(type) == 'string'
    _entities = entities
    entities = []
    for entity in _entities
      entities.push(entity) if entity instanceof type

  for entity in entities
    targetL = target.location if target
    entityL = entity.location

    target = entity if not target or entityL.distance(searchL) < targetL.distance(searchL)

  return target

getEntity = (uid, world) ->
  for entity, i in world.entities
    if entity.getUniqueId() == uid
      return entity
  return undefined

kill = (entity) ->
  entity = gplr entity if typeof entity == 'string'

  if entity.health?
    entity.health = 0
  else
    entity.remove()

  true
heal = (entity) ->
  entity = gplr entity if typeof entity == 'string'
  return unless entity instanceof org.bukkit.entity.LivingEntity
  entity.health = entity.maxHealth
