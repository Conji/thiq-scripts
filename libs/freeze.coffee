registerPermission 'thiq.freezeTime', 'op'
frozenTime = false
freezeTask = null

registerCommand
  name: 'freeze',
  description: 'Freezes the server time at the specified time. If none is given, it freezes at the current time.',
  usage: "\xA7e/<command> [time]",
  permission: 'thiq.freeze',
  permissionMessage: 'You do not have permission to use this.'
  (sender, label, args) ->
    if (freezeTask && freezeTask.isRunning)
      sender.sendMessage('You must use /unfreeze first before refreezing!')
      return
    frozenTime = sender.getWorld().getTime()
    if (args.length > 0 && !isNaN(args[0])) then frozenTime = parseNum(args[0])
    world = sender.getWorld();
    freezeTask = setInterval(() ->
      world.setTime(frozenTime)
    , 100)
    sender.sendMessage("World frozen at #{frozenTime}")


registerCommand
  name: 'unfreeze',
  description: 'Unfreezes the server time.',
  usage: "\xA7e/<command>",
  permission: 'thiq.freezeTime',
  permissionMessage: 'You do not have permission to use this.'
  (sender, label, args) ->
    cancelInterval(freezeTask)
