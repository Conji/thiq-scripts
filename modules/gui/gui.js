 /*
  meant to help with creating inventory based GUIS easily.
  Once created, the GUIs cannot be edited without doing a /reload
  NOTE: none of the GUIs are persisted, so you must do this yourself.
  */

 var registeredGuis = [];

 /**
  * @typedef {Object} Gui
  */
 function Gui() {
     this._id = Math.random();
     this._inventory = [];
     this._boundActions = {};
     this._watchers = [];
     this._title = 'GUI';

     /**
      * @returns {number} 
      */
     this.getInventorySize = function() {
         return this._inventory.length + 9 - (this._inventory.length % 9);
     }

     /**
      * Sets the title of the GUI.
      * @returns {Gui}
      * @param {string} title
      */
     this.setTitle = function(title) {
         this._title = title;
         return this;
     }

     /**
      * Adds an item stack to the GUI for interaction.
      * @returns {Gui}
      * @param {ItemStack} item
      * @param {{name: string, lore: string[]}} options
      * @param {function(Player)} leftClick
      * @param {function(Player)=} rightClick
      * @param {number=} index
      */
     this.add = function(item, options, leftClick, rightClick, index) {
         if (options != null) {
             var oldMeta = item.itemMeta;
             oldMeta.displayName = options.name;
             if (options.lore != undefined) {
                 if (typeof options.lore == 'string') {
                     oldMeta.setLore([lore]);
                 } else {
                     oldMeta.setLore(lore);
                 }
             }
             item.itemMeta = oldMeta;
         }
         if (index == undefined) {
             this._inventory.push(item);
         } else {
             this._inventory[index] = item;
         }
         this._boundActions[item] = { left: leftClick, right: rightClick };
         return this;
     }

     /**
      * @returns {Gui}
      * @param {ItemStack} item
      */
     this.remove = function(item) {
         this._inventory.slice(item, 0);
         this._boundActions[item] = undefined;
         return this;
     }

     /**
      * @returns {Gui}
      * @param {Player} player The player to open for.
      */
     this.open = function(player) {
         var window = Bukkit.createInventory(null, this.getInventorySize(), this._title);
         for (var i = 0; i < this._inventory.length; i++) {
             var item = this._inventory[i];
             window.setItem(i, item);
         }
         player.openInventory(window);
         this._watchers.push(player.getUniqueId());
         return this;
     }

     /**
      * @returns {Gui}
      * @param {Player} player The player to close for.
      */
     this.close = function(player) {
         this._watchers.slice(player.getUniqueId(), 0);
         player.closeInventory();
         return this;
     }

     /**
      * 
      */
     this.destroy = function() {
         registeredGuis.slice(this, 1);
     }

     registeredGuis.push(this);
     return this;
 }


 registerEvent(inventory, 'click', function(event) {
     for (var i = 0; i < registeredGuis.length; i++) {
         var gui = registeredGuis[i];
         if (event.getRawSlot() > gui.getInventorySize() - 1) continue;
         if (gui._watchers.indexOf(event.getWhoClicked().getUniqueId()) > -1) {
             if (event.getRawSlot() >= gui.getInventorySize()) return;
             var action = org.bukkit.event.inventory.InventoryAction;
             var click = org.bukkit.event.inventory.ClickType;
             if (event.getAction() == action.PLACE_ONE || event.getAction() == action.PLACE_ALL || event.getAction() == action.PLACE_SOME || event.getClick() == click.SHIFT_LEFT || event.getClick() == click.SHIFT_RIGHT) {
                 event.cancelled = true;
                 return;
             }
             var boundItem = gui._boundActions[event.getCurrentItem()];
             if (boundItem == undefined) return;
             if (event.getClick() == click.LEFT && boundItem.left) {
                 boundItem.left(event.getWhoClicked());
                 event.cancelled = true;
                 return;
             }
             if (event.getClick() == click.RIGHT && boundItem.right) {
                 boundItem.right(event.getWhoClicked());
                 event.cancelled = true;
                 return;

             }
         }
     }
 });

 registerEvent(inventory, 'close', function(event) {
     for (var i = 0; i < registeredGuis.length; i++) {
         var gui = registeredGuis[i];
         if (gui._watchers.indexOf(event.getPlayer().getUniqueId()) > -1) {

             gui._watchers.slice(event.getPlayer().getUniqueId(), 0);
             return;
         }
     }
 });

 Gui.getGui = function(id) {
     return _.find(regsiteredGuis, { _id: id });
 }
 exports = Gui;