var bukkit = importClass('org.bukkit.Bukkit');
var runnable = function(fn) {
    return new java.lang.Runnable({
        run: fn
    });
}

var async = function(fn, callback) {
    var thread = new java.lang.Thread(runnable(function() {
        var result = null;
        try {
            if (typeof(fn) == 'string') {
                result = eval(fn);
            } else {
                result = fn();
            }
        } catch (err) {
            result = err;
        }
        if (callback) {
            callback(result);
        }
    }));
    thread.start();
    return thread;
}

exports.sync = function(fn, callback, delay, args) {
    var task;
    if (!delay || delay == null) {
        delay = 1;
    }
    if (!args || args == null) {
        args = [];
    }
    var task = runnable(function() {
        var result = fn.apply(this, args);
        if (callback) {
            return async(function() {
                return callback(result);
            });
        }
    });
    return bukkit.server.schedule.scheduleSyncDelayedTask(plugin, task, delay);
}

exports = async;