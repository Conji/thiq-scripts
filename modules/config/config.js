var read_file = function(file) {
    var f_in = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(file), "UTF8"));

    var line;
    var string = "";
    while ((line = f_in.readLine()) != null) {
        string += line + '\n';
    }

    f_in.close();
    return string;
}

var save_file = function(file, data) {
    var f_out = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(file), "UTF8"));

    f_out.append(data);
    f_out.flush();
    f_out.close();
}

var Config = function(location) {
    this.objects = {};
    this.location = location;
    this.load = function() {
        var file = new java.io.File(this.location);
        if (!fs.exists(this.location)) {
            file.createNewFile();
        }

        try {
            var fdata = read_file(file);
            this.objects = JSON.parse(fdata);
            this.objects.save = this.save();
            return this.objects;
        } catch (ex) {
            log('Failed to load config at ' + this.location + ': ' + ex, 'c');
            return false;
        }
    }

    this.save = function() {
        var file = new java.io.File(this.location);

        try {
            save_file(file, JSON.stringify(this.objects, null, '\t'));
        } catch (ex) { return false; }
    }

    return this;
}

exports = Config;