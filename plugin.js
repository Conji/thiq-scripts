// All script initialization begins here. All globals are set in here.
global = this;

function loadCore(name) {
    try {

        log('Loading core library file ' + name, 'd');
    } catch (ex) {

    }
    loader.load('./plugins/Thiq/core/' + name + '.js');
}

// load core scripts
//    first load the logger
loadCore('logger');
//    then loader 
loadCore('loader');
//    then require
loadCore('require');
// load the rest of the core
loadCore('lang');
loadCore('safety');
loadCore('promise');
loadCore('tpm');
loadCore('thiq');