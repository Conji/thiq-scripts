// event handler objects
var block = {};
var enchantment = {};
var entity = {};
var inventory = {};
var hanging = {};
var player = {};
var server = {};
var vehicle = {};
var weather = {};
var world = {};
var js = {};

Object.prototype.toString = function() { try { return JSON.stringify(this, ' ', '\t').replace(/\t/g, "  "); } catch (e) { return "[object Object]" } }
Array.prototype.toString = function() { try { return JSON.stringify(this, ' ', '\t').replace(/\t/g, "  "); } catch (e) { return "[object Array]" } }

var Bukkit = org.bukkit.Bukkit;

/**
 * Registers an event for the plugin to handle, returning the cancel token of the callback.
 * All scripts can use this.
 * @param {Object} handler 
 * @param {string} eventname 
 * @param {function(Event)} callback 
 */
function registerEvent(handler, eventname, callback) {
    callback.cancelToken = new java.lang.Object();
    if (handler[eventname]) {
        handler[eventname].callbacks.splice(0, 0, callback);
    } else {
        handler[eventname] = function(event) {
            var callbacks = handler[eventname].callbacks;
            for (var i = 0; i < callbacks.length; ++i) {
                try {
                    callbacks[i](event);
                } catch (ex) {
                    if (event instanceof org.bukkit.event.player.PlayerEvent && typeof(ex) === 'string') {
                        event.player.sendMessage(ex);
                    }
                    log(ex);
                }
            }
        };
        handler[eventname].callbacks = [];
        handler[eventname].callbacks.push(callback);
    }
    return callback.cancelToken;
}

/**
 * Unregisters an event to be handled. All scripts can use this.
 * @param {Object} handler 
 * @param {string} eventname 
 * @param {Object} cancelToken 
 */
function unregisterEvent(handler, eventname, cancelToken) {
    list = handler[eventname].callbacks;
    for (var i = 0; i < list.length; ++i) {
        if (list[i].cancelToken == cancelToken) {
            list.splice(i, 1);
            return true;
        }
    }
    return false;
}

/**
 * Registers a permission. 
 * @param {string} permission 
 * @param {*} perm_default: Can be true for anybody to use it, false for permission only, or 'op' for operator only.
 * @param {string[]} parents 
 */
function registerPermission(permission, perm_default, parents) {
    try {
        if (loader.server.pluginManager.getPermission(permission)) {
            loader.server.pluginManager.removePermission(loader.server.pluginManager.getPermission(permission));
        }

        var perm_default = org.bukkit.permissions.PermissionDefault[perm_default.toUpperCase()]
        var permission = new org.bukkit.permissions.Permission(permission, perm_default);

        if (parents) {
            for (var i in parents) {
                permission.addParent(parents[i].permission, parents[i].value);
            }
        }

        loader.server.pluginManager.addPermission(permission);
        return permission;
    } catch (ex) {
        log(ex);
        return null;
    }
}

/**
 * Registers a command for usage. All scripts can use this.
 * @param {{name:string,usage:string,permission:Permission,permissionMessage:string,description:string,aliases:string[]}} data 
 * @param {function(Object,string,string)} func 
 */
function registerCommand(data, func) {
    var commandExecClass = plugin["class"].classLoader.loadClass("net.conji.thiq.JsCommandExecutor");
    var newCommand = function() {
        var commandClass = plugin["class"].classLoader.loadClass("net.conji.thiq.JsCommand");
        var commandEx = (function(__func__) {
            var state = { func: __func__, permTest: function(p) { return true; } };
            var object = {
                execute: function(sender, label, args) {
                    if (!this.getState().permTest(sender)) {
                        return true;
                    }
                    try {
                        label = _s(label);
                        args = stringArray(_a(args));
                        var flags = [];
                        if (data.flags) {
                            for (var i in args) {
                                if (/^\-([a-z]+)/i.test(args[i])) {
                                    for (var k = 1; k < args[i].length; ++k) {
                                        flags.push(args[i][k]);
                                    }
                                    args.splice(i--, 1);
                                }
                            }
                        }
                        var result = this.getState().func(sender, label, args, flags);
                        if (result == false) {
                            sender.sendMessage(data.usage.replace(/\<command\>/g, _s(label)));
                        };
                        return true;
                    } catch (ex) {
                        if (typeof(ex) == 'string') {
                            sender.sendMessage("\xA7c" + ex);
                        } else {
                            sender.sendMessage("\xA7cAn error occured executing your command");
                            log(ex, 'c');
                        }
                        return true;
                    }
                },
                getState: function() { return state; }
            };
            return loader.Interface(object, commandClass);
        })(func);

        data.description = data.description || "";
        data.usage = data.usage || "";
        data.aliases = data.aliases || [];

        var cmd = plugin.createCommand(data.name, data.description, data.usage, data.aliases, commandEx);

        if (data.permission) {
            cmd.permission = data.permission;
            if (data.permissionMessage) {
                cmd.executor.state.permTest = function(p) {
                    if (cmd.testPermissionSilent(p)) {
                        return true;
                    } else {
                        p.sendMessage(data.permissionMessage);
                        return false;
                    }
                }
            } else {
                cmd.executor.state.permTest = function(p) {
                    return cmd.testPermission(p);
                }
            }
        }

        return cmd;
    }

    var command;
    if ((command = loader.server.commandMap.getCommand(data.name)) && command["class"].equals(commandExecClass)) {
        log("Unregistering old command '" + data.name + "'", 'e', 'verbose');

    }
    log("Registering new command '" + data.name + "'", 'e', 'verbose');
    loader.server.commandMap.register("js", newCommand());
}

/**
 * [DEPRACATED] Unregisters a command within Spigot.All plugins can use this.
 * @param {string} name 
 */
function unregisterCommand(name) {

}

/**
 * Returns a plugin with the passed name. If no argument, then it returns Thiq.
 * @param {string=} name 
 */
function getPlugin(name) {
    if (typeof(name) !== 'string') {
        name = "Thiq";
    }
    return loader.server.pluginManager.getPlugin(name);
}

var plugin = getPlugin();

function read_proc() {
    var pb = new java.lang.ProcessBuilder["(java.lang.String[])"](_a(arguments));
    pb.redirectErrorStream(true);
    var proc = pb.start();
    var out = proc.inputStream;
    var reader = new java.io.BufferedReader(new java.io.InputStreamReader(out));
    var output = "";
    var line;
    while ((line = reader.readLine()) != null) {
        output += line + "\n";
    }
    return output;
}

function evalScript(javascript) {
    try {
        return engine.eval(javascript);
    } catch (exception) {
        var ex = exception;
        if (ex.sciptExcetion) {
            ex = ex.sciptExcetion;
            while (ex && ex.unwrap) {
                ex = ex.unwrap().cause;
            }
            ex = (ex || { message: "Unknown error" }).message;
        }

        log(ex);
        if (/syntax error/i.test(ex)) {
            throw "\n" + javascript;
        } else {
            throw exception;
        }
    }
}

function callEvent(handler, event, data) {
    if (handler[event]) {
        handler[event](data);
    }
}

function cmdEval(message, sender, type) {
    currEvalPlr = sender;
    try {
        var event = {
            sender: sender,
            type: type,
            ext: {}
        }
        callEvent(js, "extensions", event);
        var result;
        with(event.ext) {
            result = evalScript(message);
        }
        callEvent(js, "evalComplete", {
            sender: sender,
            result: result
        });
        if (result === undefined) {
            result = "undefined";
        } else if (result === null) {
            result = "null";
        }
        sender.sendMessage("\xA7a=> " + result);
        return result;
    } catch (ex) {
        sender.sendMessage("\xA7c" + ex);
        return undefined;
    }
}
var currEvalPlr;

function loadLibraryScript(name, loader) {
    var loaderData = {
        name: 'js',
        ext: '.js',
        compileFn: function(js) { return js; },
        options: {}
    };
    if (loader != undefined) {
        loaderData = registeredLoaders[loader];
    }

    log('Loading library script ' + name + ' using the ' + loaderData.name + ' loader.', 'd');
    if (fs.exists('./plugins/Thiq/libs/' + name + loaderData.ext)) {
        loadFromFile('./plugins/Thiq/libs/' + name + loaderData.ext, loaderData.compileFn, loaderData.options);
    } else {
        log('Could not locate library script ' + name, 'c');
    }
}

registerCommand({
    name: "js",
    description: "Executes javascript in the server",
    usage: "\xA7cUsage: /<command> [javascript code]",
    permission: registerPermission("thiq.js", "op"),
    permissionMessage: "\xA7cYou don't have permission to use that!",
    aliases: ["javascript"]
}, function(sender, label, args) {
    var message = args.join(" ");

    if (message.length < 1) {
        return false;
    }

    message = message.replace(/\{clipboard\}/i, sender.clipboardText);

    sender.sendMessage("\xA77>> " + message);

    cmdEval(message, sender, "js");
});

var registeredLoaders = {};

/**
 * 
 * @param {string} name The name of the loader.
 * @param {function(string)} compileFn The function that compiles and returns the code as a string.
 * @param {string} ext The filename extension.
 */
function registerLoader(name, compileFn, options, ext) {
    registeredLoaders[name] = { name: name, compileFn: compileFn, ext: ext, options: options };
}

var config = require('config');
var fs = require('fs');

libraryConfig = new config('./plugins/Thiq/thiq.json').load();
// load block IDs now
var blocks = JSON.parse(fs.readFileSync('./plugins/Thiq/core/data.json'));

log('Loading loaders...', 'd');
// load the loaders
for (var i = 0; i < libraryConfig.loaders.length; i++) {
    loadFromFile('./plugins/Thiq/loaders/' + libraryConfig.loaders[i]);
    log('Found loader ' + libraryConfig.loaders[i], 'd');
}

log('Loading libraries...', 'd');
// load the lib files
for (var i = 0; i < libraryConfig.libraries.length; i++) {
    var value = libraryConfig.libraries[i];
    if (typeof value == 'string') {
        loadLibraryScript(value);
    } else if (typeof value == 'object') {
        loadLibraryScript(value.file, value.loader);
    }

}