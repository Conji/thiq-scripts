var BufferedReader = importClass('java.io.BufferedReader');
var InputStreamReader = importClass('java.io.InputStreamReader');
var FileInputStream = importClass('java.io.FileInputStream');
var BufferedWriter = importClass('java.io.BufferedWriter');
var OutputStreamWriter = importClass('java.io.OutputStreamWriter');
var FileOutputStream = importClass('java.io.FileOutputStream');
var File = importClass('java.io.File');

var loadedPreprocessors = [];

// adds a preprocessor to the loader.
// processor: function(fileLocation) returns a string of the new javascript to be passed.
function addPreprocessor(processor) {
    // TODO: safety check?
    loadedPreprocessors.push(processor);
}

function _readFile(location) {
    var fIn = new BufferedReader(new InputStreamReader(new FileInputStream(location), "UTF8"));

    var line;
    var string = "";
    while ((line = fIn.readLine()) != null) {
        string += line + '\n';
    }

    fIn.close();
    return string;
}

function eval(script) {
    return engine.eval(script);
}

function importClass(javaClass) {
    return Java.type(javaClass);
}

// the loader will first use all preprocessors before evaluating the script.
// if the preprocessor arg is not undefined, it will process only using that one.
function load(javascript, preprocessor, options) {
    try {
        if (preprocessor == undefined) {
            return engine.eval(javascript);
        } else {
            var parameters = [
                javascript,
                options
            ];
            var result = preprocessor.apply(this, parameters);
            return engine.eval(result);
        }
    } catch (exception) {
        log('Failed to process JavaScript: ' + exception, 'c');
    }
}

function loadFromFile(file, preprocessor, options) {
    var result = _readFile(file);
    return load(result, preprocessor, options);
}