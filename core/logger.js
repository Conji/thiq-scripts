Logger = (function() {
    this.debugMessages = true;
    this.log = function(msg, level, verbose) {
        if (verbose && !this.debugMessages) return;
        if (!level) level = "f";
        if (msg instanceof Array) {
            for (var i in msg) {
                loader.server.consoleSender.sendMessage("\xA7" + level + "[Thiq] " + msg[i]);
            }
        } else {
            loader.server.consoleSender.sendMessage("\xA7" + level + "[Thiq] " + msg);
        }
    }

    return this;
}());

function log(msg, level, verbose) {
    Logger.log(msg, level, verbose);
}