/*
the module loading system for Thiq. 
*/

var importFactories = [];
var defined = {};
var baseScriptLocation = './plugins/Thiq/';

// adds a factory to assist in require if the module can't be loaded
function addImportFactory(factory) {
    importFactories.push(factory);
}

function Version(major, min, sub) {
    if (typeof major == 'string' && !min && !sub) {
        // using a string to define the version
        var v = major.split('.');
        this.major = parseNum(v[0]);
        this.major = parseNum(v[1] || 0);
        this.sub = parseNum(v[2] || 0);
    } else {
        this.major = major;
        this.minor = min;
        this.sub = sub;
    }
}

function _isQualifyingVersion(version, requestedVersion) {
    var requiresMin = requestedVersion.endsWith('+');
    var requiresMax = requestedVersion.endsWith('-');
    // TODO:
    return true;
}

function loadWithWrapper(module, file) {
    var data = _readFile(file);
    var head = '(function(exports, require){\r\n';
    var tail = '\r\nreturn exports;})';
    var code = head + data + tail;
    var compiledFn = engine.eval(code);
    var moduleInfo = {
        exports: {}
    }
    var parameters = [
        moduleInfo.exports,
        require
    ];
    var result = compiledFn.apply(moduleInfo.exports, parameters);
    // TODO: parent files
    defined[module] = result;
    return result;
}

function require(lib) {
    if (defined[lib] != undefined && defined[lib] != null) {
        return defined[lib];
    }
    // check in local modules
    if (new java.io.File(baseScriptLocation + 'modules/' + lib).exists()) {
        if (new java.io.File(baseScriptLocation + 'modules/' + lib + '/index.js').exists()) { // load index.js
            return loadWithWrapper(lib, baseScriptLocation + 'modules/' + lib + '/index.js');
        } else if (new java.io.File(baseScriptLocation + '/modules/' + lib + '/' + lib + '.js').exists()) {
            return loadWithWrapper(lib, baseScriptLocation + 'modules/' + lib + '/' + lib + '.js');
        } else if (new java.io.File(baseScriptLocation + 'modules/' + lib + '/package.json').exists()) {
            var package = JSON.parse(_readFile(baseScriptLocation + 'modules/' + lib + '/package.json'));
            for (var i = 0; i < package.dependencies; i++) {
                require(package.dependencies[i]);
                // TODO: version checking
            }
            return loadWithWrapper(lib, baseScriptLocation + 'modules/' + lib + '/' + package.main);
        } else {
            // try loaded java lib as final resort.
            try {
                var javaClass = Java.type(lib);
                defined[lib] = javaClass;
                return defined[lib];
            } catch (ex) {
                throw 'Could not locate module ' + lib;
            }
        }
    } else {
        throw 'Could not locate module ' + lib;
    }
}