/* TPM library for Thiq. Allows pulling of modules and libraries for usage. */
(function() {
    var http = require('http');

    registerCommand({
        name: 'tpm',
        usage: '\xA7e/<command> <install|delete|update> <module|library> <name>',
        permission: registerPermission('thiq.tpm', 'op'),
        permissionMessage: "\xA7cYou don't have permission to use that!",
        description: 'Installs a module or library for Thiq.'
    }, function(sender, label, args) {
        if (args.length != 3) {
            sender.sendMessage('\xA7cCorrect usage is /tpm <install|delete> <module|library> <name>');
            return;
        }

        if (args[0] == 'install') {
            if (args[1] == 'module') {
                installModule(args[2]);
                return;
            } else if (args[1] == 'library') {
                installLibrary(args[2]);
                return;
            }
        } else if (args[0] == 'delete') {
            if (args[1] == 'module') {
                deleteModule(args[2]);
                return;
            } else if (args[1] == 'library') {
                deleteLibrary(args[2]);
                return;
            }
        } else if (args[0] == 'update') {
            return;
        }
        sender.sendMessage('\xA7cCorrect usage is /tpm <install|delete> <module|library> <name>');
    });

    function installModule(name) {

    }

    function installLibrary(name) {

    }

    function deleteModule(name) {

    }

    function deleteLibrary(name) {

    }
})