function arrayClone(arr, length) {
    if (length) {
        return arr.slice(0, length);
    } else {
        return arra.slice(0);
    }
}

function _s(str) {
    return "" + new String(str);
}

function arrayContains(array, element) {
    for (var i in array) {
        if (array[i] == element) {
            return true;
        }
    }
    return false;
}

function enumContains(enu, name) {
    for (var i in enu) {
        if (i == name) return true;
    }
    return false;
}

function _a(ja) {
    var newarray = [];
    if (ja instanceof java.lang.Iterable) {
        var iter = ja.iterator();
        while (iter.hasNext()) {
            newarray.push(iter.next());
        }
        return newarray;
    } else if (_a instanceof java.util.Map) {
        ja = ja.values().toArray();
    }

    for (var i = 0; i < ja.length; ++i) {
        newarray.push(ja[i]);
    }

    return newarray;
}

function stringArray(array) {
    var _new = [];
    for (var i in array) {
        _new[i] = _s(array[i]);
    }
    return _new;
}

function numArr(start, end) {
    var res = [];
    for (var i = start; i <= end; ++i) {
        res.push(i);
    }
    return res;
}

function sleep(milliseconds) {
    milliseconds = milliseconds || 0
    java.lang.Thread.sleep(milliseconds);
}

function listMembers(obj) {
    var results = [];
    for (var i in obj) {
        results.push(i);
    }
    return results;
}

function _n(num) {
    return Number(num) + 0
}

function parseNum(num) {
    return Number(num) + 0;
}

function setTimeout(func, time) {
    return async(function() {
        sleep(time);
        func();
    });
}
var registeredIntervals = []

function setInterval(func, time) {
    var intObject = { isRunning: true, time: time };
    var thread = async(function() {
        while (intObject.isRunning == true) {
            sleep(intObject.time);
            func(cancel);
        }
    });
    intObject.thread = thread;
    registeredIntervals.push(intObject);

    /**
     * Cancels the current interval. Only usable from within the setInterval function.
     */
    function cancel() {
        intObject.isRunning = false;
    }

    return intObject;
}

function pauseInterval(intObject) {
    intObject.isRunning = false;
    return intObject;
}

function resumeInterval(intObject) {
    intObject.isRunning = true;
    return intObject;
}

function cancelInterval(intObject) {
    intObject.isRunning = false;
    registeredIntervals.splice(intObject);
    return intObject;
}

function cancelAllIntervals() {
    for (var i = 0; i < registeredIntervals.length; i++) {
        cancelInterval(registeredIntervals[i]);
    }
}

function evalInContext(code, context) {
    with(context) {
        eval(_s(code));
    }
}

function numToEnum(enumObject, value) {
    return enumObject.values()[value];
}

String.isNullOrEmpty = function(input) {
    return input == undefined || input == null || input == '';
}

Object.defineProperty(Object.prototype, 'prop', {
    get: function() {
        return function(name, descriptor) {
            var constructor = this;
            Object.defineProperty(constructor.prototype, name, descriptor);
        }
    },
    enumerable: false
});

Object.defineProperty(Object.prototype, 'inherits', {
    get: function() {
        return function(parent) {
            for (var field in parent) {
                var constructor = this;
                Object.defineProperty(constructor.prototype, field, parent[field]);
            }
        }
    },
    enumerable: false
});

function constructObject(constructor, args) {
    function F() {
        return constructor.apply(this, args);
    }
    F.prototype = constructor.prototype;
    return new F();
}

Function.prop('new', {
    get: function() {
        return function() {
            return constructObject(this, arguments);
        }
    }
});

function unwrapObject(target) {
    var value = {};
    for (var field in target) {
        value[field] = target[field];
    }
    return value;
}

function extend(javaObject, options) {
    var result = Java.extend(javaObject);
    for (var property in result) {
        result[property] = options[property] || Java.super(result)[property];
    }
    return result;
}

Boolean.prototype.toUpperCase = function() {
    if (this === true) {
        return 'TRUE';
    } else {
        return 'FALSE';
    }
}